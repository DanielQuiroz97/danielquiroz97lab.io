---
title: Exploring the notame package
author: Daniel Quiroz
date: '2020-04-10'
slug: notame
categories:
  - LC-MS
tags:
  - Untarget metabolomics
  - Liquid chromatography
  - Mass spectrometry
subtitle: 'Part I: Preprocessing'
description: 'notame is an R package for untargeted metabolomic analysis. This package offers common statistical analysis functions for metabolomics such as preprocessing, feature-wise analysis, multivariate analysis and visualizations'
image: '/img/post/qc_white.png'
output:
  blogdown::html_page:
    toc: true
---

<style>
body {text-align: justify}
</style>

## Disclaimer
- <font color="e96060"> This post does not intend to replace the official
package documentation. This post aims to examine the main functions that
it provides. For official documentation, please refer to the paper or the
corresponding author. </font>

- <font color="00c018"> In order to avoid package misuse, this post
was revised for the package developer and his suggestions was already
implemented. </font>

## Notame workflow introduction
The *notame* package is documented and explained in a detailed manner in the paper
[“notame”: Workflow for Non-Targeted LC–MS Metabolic Profiling](https://doi:10.3390/metabo10040135).
This package can be found, at the moment, only at
[github](https://github.com/antonvsdata/notame) in the current 0.0.1 version.

Briefly, the notame package works with the output of the 
[MS-DIAL](http://prime.psc.riken.jp/compms/msdial/main.html) software at the
MS1 level. MS-DIAL is used to perform the peak picking and peak alignment
routines before to imports the peak list in notame package.

The notame package offers more than 100 functions to perform metabolomic
profiling analysis. This functions cover several approach such as:

- Import data
- Preprocessing
- Feature-wise analysis (Parametric and non-parametric analysis)
- Multivariate analysis
- Visualize statistical analysis and results

## Package installation

Since the package is only hosted in github, it must be installed through the
developer version with the `devtools` package.

If you do not have the `devtools` package, you can use the following commands:

```{r eval=FALSE}
install.packages("devtools")
devtools::install_github("antonvsdata/notame",
                         build_manual = T, build_vignettes = T)
```

The `install_github` function will download the package, and install it in your
computer. In this case, the `build_manual` and `build_vignettes` are set to 
`TRUE` in order to install the package documentation and available tutorials.

## MS-DIAL analysis
Although several peak picking algorithms exist in vendor and non-vendor
software, the notame package is personalized to work primarly, but not
exclusively, with MS-DIAL.

In the notame [paper](https://doi:10.3390/metabo10040135), the  parameters to be 
used in MS-DIAL from the $30^{th}$ to the $33^{rd}$ step are described.
Briefly, we include the suggested parameters with some changes based on our own
data characteristics.

![Figure 1. Parameters used for peak picking and aligment.](/post/2020-04-10-exploring-notame-package_files/MSDIAL.png){width=75%}

### Peak table modifications

Once MS-DIAL finishes the process, the alignment results has to be exported in 
a .txt file. As explained in the `project_example` vignette, you have to 
slightly modify this table and transform to an excel file. To have a big
picture about how this table should be like, notame authors provide some
[example data](https://github.com/antonvsdata/notame/tree/master/inst/extdata).

From the raw MS-DIAL output (.txt), you have to introduce some modifications, 
which most of them are renaming column, For example,

|   Old name  |  New name |
|:-------|:-------| 
| Alignment ID | Compound |
| Average Rt(min) | RT    |
| Average Mz     | Mass   |

Additionally, two columns must be added before you import your data.
A column named as *"Column"* and *"Ion Mode"* has to be created. As suggestion,
you can replace one of the column in your raw data that does not contain
information. These two columns are in case if you analyzed your samples with
multiple column types ($C_{18}$, $C_{8}$, ...) or the ionization was performed
in different modes. If you only acquired your data with one column and one ion
mode, create the column and fill all the cells with the same value. Then, export
the table in an excel file with only one sheet.

Your final table should look similar as the next table.

![Figure 2. Representation of the example data provided by the notame package authors.](/post/2020-04-10-exploring-notame-package_files/example_data.png){width=60%}

The table is composed with three embedded tables. The sample
metadata (top blue), the chromatographic information (left green) and the 
feature ambundance (middle orange).

## Notame workflow
Here we are going to load the required libraries to perform  data analysis.

```{r message=FALSE, warning=FALSE, paged.print=FALSE}
library(notame) # Workflow for non-targeted LC-MS metabolic profiling 
library(doParallel) # Foreach Parallel Adaptor for the 'parallel' Package
library(here) # A Simpler Way to Find Your Files 
library(magrittr) # A Forward-Pipe Operator for R 
library(tidyverse) # Easily Install and Load the 'Tidyverse' 
library(patchwork) # The Composer of Plots 
library(BatchCorrMetabolomics) # "Improved Batch Correction in Untargeted MS-Based Metabolomics"
```


```{r include=FALSE}
load(here("static", "data", "mode.RData"))
set.seed(10)
```


### Log file
In order to trace every process, the notame package includes a log system to
trace every single command. Therefore, users can see the log file to check
the executed commands.

```{r message=FALSE, warning=FALSE, paged.print=FALSE, eval=FALSE}
ppath <- here()
init_log(log_file = paste0(ppath, 'log.txt'))
```


### Importing data
Data can be imported with the `read_from_excel` function. The default argument values
are intuitive to read the data only with one extra argument than the file name.

```{r eval=FALSE}
data <- read_from_excel(file = here("static", "data", "peak_noblanck.xlsx"), 
                        split_by = c("Column", "Ion mode"))
```

Once the data is read, a *MetaboSet* has to be created in order to create a
specific R object (S4). At the first glance, the MetaboSet is composed by three
tables:

- *exprs*: The expression table which contains the feature abundance,
- *pheno_data*: the phenotype or a more coloquial, the metadata, and
- *feature_data*: The chromotographic information such as rt, m/z, etc.

![Figure 3. MetaboSet architecture.](/post/2020-04-10-exploring-notame-package_files/Metaboset.jpg){width=60%}

The MetaboSet object is created as follow:

```{r eval=FALSE}
modes <- construct_metabosets(exprs = data$exprs, pheno_data = data$pheno_data,
                              feature_data = data$feature_data, group_col = "Class")
```

**Note**: the `group_col` argument is a string with the column name of the 
sample class, ensure to provide the correct column name based on the Fig. 2.

FInally, each mode (set of columns and ion mode) can be extracted in a single 
object.

```{r eval=FALSE}
mode <- modes$Octodecyl_Pos
```

As you can probably notice, this samples was acquiered with a $C_{18}$ column
in a positive ionization mode.

In the notame package, the function `visualizations` create all available figures
for the MetaboSet object. Although, in this document this function is not use,
I totally encourage to use it in order to inspect the processing routines.


```{r message=FALSE, warning=FALSE, paged.print=FALSE}
raw_sambx <- plot_sample_boxplots(mode, order_by = "Injection_order")
raw_pca <- plot_pca(mode, center = T, color = "Batch_ID") 
raw_pca + raw_sambx + plot_layout(guides = "collect")
```

The Principal Component Analysis (PCA) in the left shows a clear separation
between the samples classes. I would like to make emphasis in the QC injections
which are not properly cluster in a common principal component space. They
were projected with a descended slope trend. This is a clear example of the lack
of drift correction and common issues as batch correction. Additionally,
as example of the influence of the injection order in the acquired mass
spectra, the boxplot has a descendant trend in the abundance of the features.

### Preprocessing
The first step of the preprocessing is to change the features with value equal
to 0 to `NA`, since the default behavior of the MS-DIAL is to replace `NA` 
with 0.

```{r}
mode <- mark_nas(mode, value = 0)
```

Then, features with low detection rate are first detected and then removed.
The notame package employs two criteria to select this features. First, is the
feature presence in a percentage of QC injections, and then the feature presence
in a percentage within a sample group or class. For example, in the next 
command, features which that were not detected in the 75% of the QC injections
and 80% of sample groups will be discarded.


```{r}
mode <- flag_detection(mode, qc_limit = 0.75, group_limit = 0.8)
```

Once the features with low detection rate are detected, the correction drift can
be applied by smoothed cubic spline regression. It is done for each
feature.

```{r}
dc <- dc_cubic_spline(mode)
corrected <- dc$object
corrected <- flag_quality(corrected)
```

```{r message=FALSE, warning=FALSE, paged.print=FALSE}
corr_sambx <- plot_sample_boxplots(corrected, order_by = "Injection_order")
corr_pca <- plot_pca(corrected, center = T) 
corr_pca + corr_sambx + plot_layout(guides = "collect")
```


### Batch correction

The batch correction implemented in the notame package is supported by the 
[batchCorr](https://gitlab.com/CarlBrunius/batchCorr/),
[RUVseq](https://bioconductor.org/packages/release/bioc/html/RUVSeq.html),
and [BatchCorrMetabolomics](https://github.com/rwehrens/BatchCorrMetabolomics) packages.
The later package corrects the batch effect by fitting ancova models, with batches
as factorial information. Additionally, different regression models are
available, such as robust linear regression `method = "rlm"` and
censored regression `method = "tobit"`. The
`BatchCorrMetabolomics` package is detailed described in the 
[Roben Wehrens and collegues paper](https://www.ncbi.nlm.nih.gom/pmc/articles/PMC4796354/).

In this case, the robust linear regression will be used in order to avoid the
possible outlaiers that may have considerable effect in the linear model.

```{r}
corrected_batch <- BatchCorrMetabolomics::doBC(object = corrected,
                                               batch = "Batch_ID",
                                               ref = "QC", ref_label = "QC",
                                               method = "rlm")
```


```{r message=FALSE, warning=FALSE, paged.print=TRUE}
batch_sambx <- plot_sample_boxplots(corrected_batch, order_by = "Injection_order")
batch_pca <- plot_pca(corrected_batch, center = T, color = "Batch_ID", shape = "Class") 
batch_pca + batch_sambx + plot_layout(guides = "collect")
```

The PCA may not be the best approach to evaluate the impact of the batch
correction. In this case, hierarchical clustering will be used to assess the 
batch correction. 

```{r}
plot_dendrogram(corrected)
```

This dendogram is performed with a not batch corrected data. The batch
injections are clustered in the right in an upper level.


```{r}
plot_dendrogram(corrected_batch)
```

The main difference with the batch corrected data is the batch injections are
clustered in a lower dendogram level. This is a clear example of the batch 
correction affects the similarity of batch injections, which lead to a deeper 
dendogram level.

I would like to finis this post at this point since the QC injections helped to 
evaluate the impact of each preprocessing routine. In the next post, the QC
will be dropped to continue with the analysis.
