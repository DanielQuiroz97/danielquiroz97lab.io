---
title: 2DGC chromatogram visualization
author: Daniel Quiroz
date: '2020-04-04'
description: "In this post we introduce the visualization techniques employed in the RGCxGC package in order to display GCxGC chromatograms acquiered with different mass analyzers"
slug: plotting-chromatograms
categories:
  - RGCxGC
tags:
  - GCxGC
  - gas chromatography
  - mass spectrometry
subtitle: 'Step by step two-dimensional chromatograms visualization'
image: '/img/plot.jpg'
output:
  blogdown::html_page:
    toc: true
---


<div id="TOC">
<ul>
<li><a href="#introduction" id="toc-introduction">Introduction</a></li>
<li><a href="#understanding-gcxgc-class" id="toc-understanding-gcxgc-class">Understanding GCxGC class</a></li>
<li><a href="#basic-chromatogram-plot" id="toc-basic-chromatogram-plot">Basic chromatogram plot</a></li>
<li><a href="#tof-vs.-quadrupole" id="toc-tof-vs.-quadrupole">TOF vs. Quadrupole</a>
<ul>
<li><a href="#chromatogram-dephasing" id="toc-chromatogram-dephasing">Chromatogram dephasing</a></li>
<li><a href="#changing-the-type-of-plot" id="toc-changing-the-type-of-plot">Changing the type of plot</a></li>
<li><a href="#creating-a-personalized-color-palette" id="toc-creating-a-personalized-color-palette">Creating a personalized color palette</a></li>
<li><a href="#changing-the-number-of-levels" id="toc-changing-the-number-of-levels">Changing the number of levels</a></li>
</ul></li>
</ul>
</div>

<style>
body {text-align: justify}
</style>
<div id="introduction" class="section level1">
<h1>Introduction</h1>
<p>The traditional chromatography provides two type of signals, (a) retention
time and (b) signal intensity. The sampling rate (MHz) defines the number
of points that the detector is capable to measure per second. Each discrete
point contributes to create a continuous time signal. A single run can be
described as <span class="math inline">\(A_{i}\)</span> where <span class="math inline">\(i\)</span> is the number of scan points in the
chromatographic run.</p>
<p>This is not the case of comprehensive chromatography (GCxGC). In the case of
GCxGC, there is a second column connected with the first column
by a modulator. The chemistry of the second column is usually different
than the first column, some authors claims this difference as an
orthogonal separation.
Although, it can not be always achieved since the chemistry of both columns
is not 100% opposite. Therefore, the second column is considered as a
second chromatographic dimension. Thus, a single two-dimensional chromatogram
can be described as a set of modulations <span class="math inline">\(k\)</span>, where each modulation has a
set of scans <span class="math inline">\(i\)</span>. Then, a 2D chromatogram can be described as <span class="math inline">\(A_{i,k}\)</span>.</p>
<p>The new structure dimensions (<span class="math inline">\(A_{i,k}\)</span>) requires a different display
layout than the most common XY-plot. There are two approaches to handle this
issue: contour based plots or three dimensional plots. In the current
package version, 3D plots are not supported. Even though, we provide
contour based plots to visualize your chromatograms.</p>
</div>
<div id="understanding-gcxgc-class" class="section level1">
<h1>Understanding GCxGC class</h1>
<p>The RGCxGC package creates a S4 data format, and one chromatogram can
storage multiple information in a single R object. For example:</p>
<pre class="r"><code>library(RGCxGC)
chrom_08 &lt;- system.file(&quot;extdata&quot;, &quot;08GB.cdf&quot;, package = &quot;RGCxGC&quot;)
MTBLS08 &lt;- read_chrom(chrom_08, mod_time = 5, verbose = F)
slotNames(MTBLS08)</code></pre>
<pre><code>## [1] &quot;chromatogram&quot; &quot;time&quot;         &quot;name&quot;         &quot;mod_time&quot;</code></pre>
<p>the <em>MTBLS08</em> chromatogram has four slots:</p>
<ul>
<li><strong>chromatogram</strong>: Two-dimensional matrix</li>
<li><strong>time</strong>: the time range of the chromatographic run</li>
<li><strong>name</strong>: the chromatogram name</li>
<li><strong>mod_time</strong>: the modulation time of the second dimension.</li>
</ul>
<p>In this library, the <code>RGCxGC::plot()</code> function will retrieve the
chromatogram slot in order to plot the chromatogram. Additionally, the plot
functions also uses the time and mod_time slot to set the plot axis.</p>
</div>
<div id="basic-chromatogram-plot" class="section level1">
<h1>Basic chromatogram plot</h1>
<p>The following chromatogram is retrieved from
<a href="https://www.ebi.ac.uk/metabolights/">MetaboLights</a> database with
the <a href="https://www.ebi.ac.uk/metabolights/MTBLS579">MTBLS579</a>
identifier, the file names is 08_GC.cdf.</p>
<p>The provided <code>RGCxGC::plot()</code> is built in the <code>graphics::contour</code> and
<code>graphics::filled.contour()</code> functions. By default, the <code>RGCxGC::plot()</code>
will use the filled contour approach.</p>
<pre class="r"><code>plot(MTBLS08)</code></pre>
<p><img src="/post/2020-04-04-plotting-chromatograms_files/figure-html/default-1.png" width="672" style="display: block; margin: auto;" /></p>
<p>The color palette of this chromatogram might not be suitable. One of
the most popular color palette employed in chromatogram visualization comes
from MATLAB. The <code>colorRamps</code> package includes two palettes to overcome this
issue, the <code>matlab.like</code> and <code>matlab.like2</code> functions.</p>
<p>Before to change the chromatogram color palette, we would like to make emphasis
that the back-end function employed in this plot, is the <code>filled.contour</code>
function. To change the palette, we have to provide the function that creates
the palette.</p>
<pre class="r"><code>library(colorRamps)
plot(MTBLS08, type = &quot;f&quot;, color.palette = matlab.like,
     main =  &quot;matlab.like&quot;)</code></pre>
<p><img src="/post/2020-04-04-plotting-chromatograms_files/figure-html/color_palette-1.png" width="672" style="display: block; margin: auto;" /></p>
<pre class="r"><code>plot(MTBLS08, type = &quot;f&quot;, color.palette = matlab.like2,
     main =  &quot;matlab.like2&quot;)</code></pre>
<p><img src="/post/2020-04-04-plotting-chromatograms_files/figure-html/color_palette-2.png" width="672" style="display: block; margin: auto;" /></p>
<p>We spot some similarities between this two palettes. First of all, the
palette button and upper limits are very similar. Even though, the
<code>matlab.like2</code> function has more color breaks, what in this case,
makes the chromatogram to look better. For example, in the <code>matlab.like</code>
palette the variation of lower signals intensities is obscured by a blue
degradation. On the other hand, the <code>matlab.like</code> palette highlight the
background variation. Moreover, the color palette
is highly dependent of the chromatogram as we are going to detail in the
following sections.</p>
</div>
<div id="tof-vs.-quadrupole" class="section level1">
<h1>TOF vs. Quadrupole</h1>
<p>The MS detector comes in some flavors, which two of the most common
mass analyzers are Time of flight and Quadrupole. They are very different about
how they work. For example, TOF mas analyzer can acquire
spectra in high resolution meanwhile the quadrupole mass analyzer is not
capable of. Additionally, the sampling rate achieved by TOF is higher than
quadrupole. Therefore, chromatograms acquired with TOF mass analyzer are
usually dimensionally higher (<span class="math inline">\(&gt; i\)</span>).</p>
<p>The previous chromatogram was acquired with TOF mass analyzer, thus
TIC intensities are about <span class="math inline">\(4x10^5\)</span> and background intensities are
about <span class="math inline">\(1x10^5\)</span>. This is not the case of the quadrupole.</p>
<p>The following chromatogram is retrieved from the package
<a href="https://doi.org/10.1016/j.microc.2020.104830">article</a>. In contrast
with the previous chromatogram, the next chromatogram was acquired with
a quadrupole mas analyzer.</p>
<pre class="r"><code>myl_d5 &lt;- system.file(&quot;extdata&quot;, &quot;08GB.cdf&quot;, package = &quot;RGCxGC&quot;)
myl &lt;- read_chrom(myl_d5, mod_time = 5, sam_rate = 25, verbose = F)
plot(myl, color.palette = matlab.like2 )</code></pre>
<p><img src="/post/2020-04-04-plotting-chromatograms_files/figure-html/unnamed-chunk-1-1.png" width="672" style="display: block; margin: auto;" /></p>
<p>Moreover, the chromatogram included in the paper is very different.</p>
<div class="float">
<img src="/post/2020-04-04-plotting-chromatograms_files/may_chrom.jpg" style="width:50.0%;height:50.0%" alt="Chromatogram reported in the RGCxGC paper" />
<div class="figcaption">Chromatogram reported in the RGCxGC paper</div>
</div>
<p>The first difference is the number of features presented in both
chromatograms. In the second chromatogram there are much more features.
Additionally, the peaks are traslocated and they are not in the same
retention time in the second dimension.</p>
<p>We are going to address this issues in four steps:</p>
<ul>
<li>Chromatogram dephasing</li>
<li>Changing the type of the plot</li>
<li>Creating a personalized color palette</li>
<li>Changing the number of levels (breaks)</li>
</ul>
<div id="chromatogram-dephasing" class="section level2">
<h2>Chromatogram dephasing</h2>
<p>In two-dimensional chromartography, the modulation time may lead a
peak partition. It is because the modulator does not inject the total
peak volume in the modulation <span class="math inline">\((i)\)</span>, and reinjects the remaining peak volume in the
next modulation <span class="math inline">\((i+1)\)</span>. The first chromatogram that we plot in the section 4
presents this challenge. Therefore, in order to improve the chromatogram
visualization and interpretation, the chromatogram is scrolled in the second
dimension.</p>
<pre class="r"><code>myl_deph &lt;- dephase_chrom(myl, rel_dephase = 65)
plot(myl_deph, color.palette = matlab.like2 )</code></pre>
<p><img src="/post/2020-04-04-plotting-chromatograms_files/figure-html/unnamed-chunk-2-1.png" width="672" /></p>
<p>The <code>dephase_chrom</code> scrolls the chromatogram by a given scrolling percentage.
In this example, the second retention time has been scrolled 75%. We
notice that the higher intensities are now in the same retention times.</p>
</div>
<div id="changing-the-type-of-plot" class="section level2">
<h2>Changing the type of plot</h2>
<p>As aforementioned, the default plot type is the filled contour. Even though,
compound concentrations is usually uneven in untargeted analysis. Therefore
compounds in a low concentration are not going to be displayed with the
current approach. In this case, we can opt for the <code>contour</code> plot. The
major difference between these two types of plots is how you provide
colors. In this case, you have to specify the colors, and not the function
that creates those colors, and you have to specify <code>type = "c"</code>.</p>
<pre class="r"><code>plot(myl_deph, type = &quot;c&quot;,col = matlab.like2(10) )</code></pre>
<p><img src="/post/2020-04-04-plotting-chromatograms_files/figure-html/unnamed-chunk-3-1.png" width="672" />
We start noticing that more features came out. This is because low signals
were obscured by the background signal.</p>
</div>
<div id="creating-a-personalized-color-palette" class="section level2">
<h2>Creating a personalized color palette</h2>
<p>Although the <code>matlab.like</code> and <code>matlab.like2</code> functions provides a fast
solution for colors, other purposes may need other colors. Thus,
we are going to explain how to create a personalized color palette.</p>
<p>Basically, the <code>colorRampPalette</code> function will be use. This function
create a color palette function with the provided colors. The provided
colors will be used as breaks.</p>
<pre class="r"><code>my_palette &lt;- colorRampPalette(rev(c(&quot;red&quot;,&quot;yellow&quot;,&quot;springgreen&quot;,
                                     &quot;blue&quot;, &quot;white&quot;)))
plot(myl_deph, type = &quot;c&quot;, col = my_palette(10) )</code></pre>
<p><img src="/post/2020-04-04-plotting-chromatograms_files/figure-html/unnamed-chunk-4-1.png" width="672" /></p>
<p>With the customized <code>my_palette</code> function the chromatogram visualization was
improved. We are almost done but more low concentration compounds are till
obscured.</p>
</div>
<div id="changing-the-number-of-levels" class="section level2">
<h2>Changing the number of levels</h2>
<p>The number of levels is a critical argument to take into consideration to
create an effective chromatogram visualization. This argument affect in how
many breaks the signal will be splitted. For example, if <code>nlevels = 10</code> the
total TIC signal will be splitted in 10 equally spaced breaks. If you
set this argument to a greater value, the total TIC signal will be splitted
in more breaks, which enhance the feature visualization.</p>
<pre class="r"><code>plot(myl_deph, type = &quot;c&quot;, col = my_palette(35), nlevels = 100 )</code></pre>
<p><img src="/post/2020-04-04-plotting-chromatograms_files/figure-html/unnamed-chunk-5-1.png" width="672" /></p>
<p>As we notice, now we have a similar chromatogram with the reported in the
paper.</p>
<p><strong>Tip</strong>: in this case of plots, the number of levels and the number of colors
should be the equal for low levels (nlevels &lt; 50). On the other hand,
for nlevels &gt; 50, nlevels should be twice or three times the number of colors.</p>
</div>
</div>
